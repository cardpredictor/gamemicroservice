import HttpStatus from 'http-status-codes';
import { WalletClient } from '../client';
import { ResponseObject } from '../types/ResponseObject';

const walletClient = new WalletClient();

export async function authorized(req: any, res: any, next: any) {
  const token = req.header('authorization') || req.headers.authorization;

  let user = null;
  let walletData = null;
  if (token) {
    try {
      const result: ResponseObject = await walletClient
        .callPromise('GET', '/users/verify', {}, {'authorization': `${token}`});

      if (result.status !== 200) {
        res.status(result.status);
        res.send(result.message);
        return result
      }

      user = result.data.user;
      walletData = result.data.wallet;
    } catch (error) {
      res.status(HttpStatus.UNAUTHORIZED);
      res.send({
        status: HttpStatus.UNAUTHORIZED,
        message: 'Invalid token'
      });

      return;
    }
  }

  if (!token || !user) {
    res.status(HttpStatus.UNAUTHORIZED);
    res.send({
      status: HttpStatus.UNAUTHORIZED,
      message: 'Invalid token'
    });

    return;
  }

  req.authorizedUser = user;
  req.walletData = walletData;

  // The user is authorised so continue
  return next();
}
