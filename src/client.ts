import request from 'request';
import { ResponseObject } from './types/ResponseObject';
import config from 'config';

const port = config.get('walletMicroservice.port');
const host = config.get('walletMicroservice.host');
const schema = config.get('walletMicroservice.schema');

// init wallet microservice url
const serviceUri = `${schema}://${host}${ port ? `:${port}` : '' }`;

export class WalletClient {
  callPromise(method: string, path:string, body: any, headers: any): Promise<ResponseObject> {
    return new Promise((resolve, reject) => {
      request(`${ serviceUri }${ path }`, {
        method,
        json: body,
        headers
      }, (err: any, res: any) => {
        if (err)
          reject(err);

        const response = typeof res.body !== 'object' ? JSON.parse(res.body) : res.body;

        if (response.status === 200)
          resolve(response);

        reject(response)
      })
    });
  }
}
