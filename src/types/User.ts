export interface User {
  id?: number
  username?: string | undefined
  password?: string | undefined
  publicKey?: string | undefined
  privateKey?: string | undefined
  walletKey?: string | undefined
}