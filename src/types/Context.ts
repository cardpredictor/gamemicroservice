import { GameRepository } from '../repository/game';
import { BetRepository } from '../repository/bet';

export interface Context {
  gameRepository: GameRepository,
  betRepository: BetRepository
}