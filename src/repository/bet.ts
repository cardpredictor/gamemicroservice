import models from '../../models'
import { UserToGame } from '../types/UserToGame';

// @ts-ignore
const betModel = models.user_to_game;

export class BetRepository {

  async create({ gameId, bet, userId }: UserToGame) {
    const result = await betModel.create({
      gameId,
      bet,
      userId
    });

    if (!result || result[0] === 0) {
      return 0;
    }

    return result[0];
  }

  async update({ gameId, bet, userId, isWin }: UserToGame) {
    const fields: any = {};

    if (bet) {
      fields.bet = bet;
    }

    if (typeof isWin === 'boolean') {
      fields.isWin = isWin;
    }

    const result = await betModel.update(fields, {
      where: {
        gameId,
        userId
      }
    });

    if (!result || result[0] === 0) {
      return 0;
    }

    return result[0];
  }

  async getByGameIdAndUserId({ gameId, userId }: UserToGame) {
    const data = await betModel.findOne({
      where: {
        gameId,
        userId
      }
    });

    if (!data) {
      return null;
    }

    return data.dataValues;
  }
}