import HttpStatus from 'http-status-codes';
import { Context } from '../types/Context';
import { ResponseObject } from '../types/ResponseObject';
import { User } from '../types/User';
import { WalletData } from '../types/WalletData';

export class AuthorizeService {
  context: Context;

  constructor(context: Context) {
    this.context = context;
  }

  // return response after requesting middleware
  async execute(user: User, walletData: WalletData ): Promise<ResponseObject> {
    try {
      if (!user) {
        return {
          status: HttpStatus.NOT_FOUND,
          message: 'User not found'
        }
      }

      // get started game
      const startedGame = await this.context.gameRepository.getUnfinishedByUserId(user.id);

      if (startedGame) {
        return {
          status: HttpStatus.CONFLICT,
          message: 'You have an unfinished game',
          data: {
            user,
            walletData,
            startedGame
          }
        }
      }

      return {
        status: HttpStatus.OK,
        message: 'OK',
        data: {
          user,
          walletData
        }
      }
    } catch (error) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: error.message || error
      }
    }
  }
}