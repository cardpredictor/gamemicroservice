import { Context } from '../types/Context';
import HttpStatus from 'http-status-codes';
import { ResponseObject } from '../types/ResponseObject';
import { UserToGame } from '../types/UserToGame';
import { User } from '../types/User';

export class StartGameService {
  context: Context;

  constructor(context: Context) {
    this.context = context;
  }

  async execute(user: User, { bet }: UserToGame): Promise<ResponseObject> {
    try {
      if (!user) {
        return {
          status: HttpStatus.NOT_FOUND,
          message: 'User not found'
        }
      }

      // get started game
      const startedGame = await this.context.gameRepository.getUnfinishedByUserId(user.id);

      if (startedGame) {
        return {
          status: HttpStatus.CONFLICT,
          message: 'You have an unfinished game',
          data: {
            startedGame
          }
        }
      }

      const newGame = await this.context.gameRepository.create({
        name: `${ user.username }'s game`
      });

      const newBet: UserToGame = await this.context.betRepository.create({
        userId: user.id,
        bet,
        gameId: newGame.id
      });

      return {
        status: HttpStatus.OK,
        message: 'OK',
        data: {
          newGame,
          newBet
        }
      }
    } catch (error) {
      return {
        status: HttpStatus.BAD_REQUEST,
        message: error.message || error
      }
    }
  }
}