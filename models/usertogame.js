'use strict';
module.exports = (sequelize, DataTypes) => {
  const UserToGame = sequelize.define('user_to_game', {
    userId: {
      type: DataTypes.INTEGER
    },
    gameId: {
      type: DataTypes.INTEGER,
      references: {
        model: 'game',
        key: 'id'
      }
    },
    bet: DataTypes.DOUBLE,
    isWin: DataTypes.BOOLEAN
  },{});
  UserToGame.associate = function(models) {
    models.user_to_game.belongsTo(models.game, {
      foreignKey: 'gameId'
    })
  };
  return UserToGame;
};